## High Level Description

Project Demo: https://youtu.be/TnChMahTugc

For this project, I implemented an employee database program in C. The program prints a menu of options, allowing the user to perform the following operations:

1. Print the database
2. Look up an employee by ID number
3. Look up an employee by last name
4. Add an employee
5. Quit

## Implementation Details

* The program begins by reading in a file specified by the command line. The input file contains a list of employees described by the following parameters: six digit ID number, first name, last name and salary. The employee data is read into an array of employee structs then sorted by ID using the quicksort algorithm. 
* The database is printed in tabular form using formatted string printing. 
* When searching for an employee by ID number, binary search is used. 
* When searching for an employee by last name, an iterative search is used. 
* When adding an employee, error checking ensures that the first/last name are less than 64 characters, that the ID number is 6 integers, and that the salary range is between 30,000 - 150,000
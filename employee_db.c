/*
 * Program reads in file of employee data
 * Employee data includes first and last name, 6 digit ID number and Salary
 * Supports the following operations:
 * 	Print Employee Database
 * 	Lookup by ID
 * 	Lookup by lastname
 * 	Add Employee
 */


#include <stdio.h>      // the C standard I/O library
#include <stdlib.h>     // the C standard library
#include <string.h>     // the C string library
#include "readfile.h"   // file reading routines
#include <stdbool.h>

 

#define MAXFILENAME     128
#define MAXNAME         64
#define MAXNAME         64
#define MAXEMPLOYEES    1024


struct Employee{
	char first_name[MAXNAME];
	char last_name[MAXNAME];
	int id;
	int salary;
};
 


void menu(); 
void printDatabase();
int lookUpByID(struct Employee db[], int id, int low, int high );
void lookUpByLastName(char l_name[], struct Employee db[]);
void addEmployee(struct Employee db[], int number_of_employees);
void sort(struct Employee db[]);
int getNumberOfEmployees(struct Employee db[]); 
struct Employee makeEmployee(); //done
void displayEmployeeInformation(struct Employee e);
void quicksort(struct Employee db[], int first, int last);
void getFilenameFromCommandLine(char filename[], int argc, char *argv[]);
void readFileIntoDatabase(char *filename, struct Employee db[]);
void readAndPrintFile(char *filename);


int main (int argc, char *argv[]) {
	//Create employee database
	struct Employee database[MAXEMPLOYEES];
	
	//Read database from input file
	char filename[MAXFILENAME];
	getFilenameFromCommandLine(filename, argc, argv);
	readFileIntoDatabase(filename, database);
	
	//Sort and print database
	int num_employees = getNumberOfEmployees(database);
	quicksort(database,0,num_employees-1);
	printDatabase(database, getNumberOfEmployees(database) );
	
	//Display menu of options to user
	int choice;
	do{
		menu();
		scanf("\n%d", &choice);
		
		switch(choice){
		case 1: //Print database
			printDatabase(database, getNumberOfEmployees(database) );
			break;
		case 2: //Look up by id
			printf("Enter ID of employee you would like to find\n");
			int id;
			scanf("%d", &id);
			int index_of_employee = lookUpByID(database, id, 0, getNumberOfEmployees(database)-1);
			if(index_of_employee == -1)
				printf("Employee Not Found\n");
			else
				displayEmployeeInformation(database[index_of_employee]);
			break;
		case 3: //Look up by last name
			printf("Enter last name of employee you would like to find\n");
			char l_name[MAXNAME];
			scanf("%s", l_name);
			lookUpByLastName(l_name, database);
			break;
		case 4: //Add employee
			addEmployee(database,getNumberOfEmployees(database));
			break;
		case 5: //Exit.
			printf("Exiting...\nMay the force be with you.\n\n\n");
			break;
		}
	}while(choice != 5);
	
	return 0;
}





/*
 * This function prints a menu for performing operations on the database
 * @param void
 * @return void 
 *   
 */
void menu(){
	printf("------------------------------------------------------\n");
	printf("(1) Print Employee Database \n ");
	printf("(2) Lookup by ID \n ");
	printf("(3) Look up by Last Name \n ");
	printf("(4) Add an Employee \n ");
	printf("(5) Exit\n ");
	printf("------------------------------------------------------\n");
	printf("Enter your choice: \n");
}



/*
 * This function reads in contents of file and adds employee data into database 
 * This function uses the readfile.h library to open files, read ints, read doubles, and read strings
 *
 * @param *filename the filename to be read in
 * @param db employee database to store file data
 * @return void
 */
void readFileIntoDatabase(char *filename, struct Employee db[]){
    //openFile returns 0 if the file is successfully opened, and -1 if the file cannot be opened.
	int ret = openFile(filename);
	if (ret == -1) {
		printf("error: can't open %s\n", filename);
		exit(1);
	}
	
	int i = 0;
	while (ret != -1){
		int id, salary;
		char fname[MAXNAME], lname[MAXNAME];
		
		//Create employee
		struct Employee e1;
		
		//Read file contents and set employee data to file contents
		//Note readDouble, readInt, readString returns 0 on success, -1 on error or EOF
		ret = readInt(&id);
		e1.id = id;
		if (ret){
			break;
		}
		
		ret = readString(fname);
		strcpy(e1.first_name, fname);
		if (ret){
			break;
		}
		
		ret = readString(lname);
		strcpy(e1.last_name, lname);
		if (ret){
			break;
		}
		
		ret = readInt(&salary); 
		e1.salary = salary;
		if (ret){
			break;
		}
		
		//Add employee to database
		db[i] = e1;
		i++;
	}
	closeFile();
}

/*
 * This function prints the employee database in tabular format
 * followed by the total number of employees in the database
 * @param db employee database to read file data into
 * @param number_of_employees total number of employees in database
 * 				required to efficiently iterate through database array
 * @return void -
 */
void printDatabase(struct Employee db[], int number_of_employees){
	char name_label[] = "NAME";
	char salary_label[] = "SALARY";
	char ID_label[] = "ID";
	
	printf("%-32s", name_label);
	printf("%-10s", salary_label);
	printf("%-10s\n", ID_label);
	printf("------------------------------------------------------\n");

	for(int i = 0; i<number_of_employees; i++){
		struct Employee e = db[i];
		printf("%-16s", e.first_name);
		printf("%-16s", e.last_name);
		printf("$%-10d", e.salary);		
		printf("%-10d", e.id);
		printf("\n");
	}
	printf("------------------------------------------------------\n");
	printf("Total number of employees: %d \n", number_of_employees);
}



/*
 * This function uses the binary search algorithm to locate the employee with the specified id number
 * @param db employee database
 * @param id target id to look up
 * @param low start index of array (0)
 * @param high end index of array
 * @return int index of target employee; returns -1 if not found
 * 			index is stored and passed to displayEmployeeInformation in main
 * 			in main, if target employee found, information is displayed; else "not found" message is printed 
 */

int lookUpByID(struct Employee db[], int id, int low, int high ){
	if (low > high) 
		return -1;
		
	int mid = (low+high)/2;
	if(db[mid].id == id)
		return mid;
	else if(db[mid].id < id)
		return lookUpByID(db, id,mid+1,high);
	else
		return lookUpByID(db, id, low, mid-1);
}

/*
 * This function iterates through database to find employee with the specified last name
 * @param db employee database
 * @param lname target last name
 * @param db employee database
 * @return void
 * 
 * In main, if target employee found, information is displayed; else "not found" message is printed
 */
void lookUpByLastName(char l_name[], struct Employee db[]){
	bool found = false;
	int i = 0;
	while(db[i].id != 0 && !found){
		if( strcmp((db[i].last_name), l_name)==0)
			found = true;
		else
			i++;
	}
	if(found)
		displayEmployeeInformation(db[i]);
	else
		printf("Employee not found\n");
}




/*
 * This funciton displays the value of all data fields of a given employee
 * @param e desired employee
 * @return void
 */

void displayEmployeeInformation(struct Employee e){
	printf("******************************************************\n");
	char name_label[] = "NAME";
	char salary_label[] = "SALARY";
	char ID_label[] = "ID";

	printf("%-10s : %s %s \n",name_label, e.first_name, e.last_name);
	printf("%-10s : $%d \n", salary_label, e.salary);
	printf("%-10s : %d \n",ID_label, e.id);
	printf("******************************************************\n");
}


/*
 * This function adds an employee to database
 * Within function, user is prompted to enter values for employee data fields
 * If input is an invalid format, user is prompted again until the correct format is entered
 * Displays employee data and asks for confirmation before officially entering employee into database
 * Sorts employee database in ascending order by id
 * @param db employee database
 * @param number_of_employees number of employees in database
 * @return void
 *
 */

void addEmployee(struct Employee db[], int number_of_employees){
	char answer = 'n';
	struct Employee e1;
	
	while(answer != 'y'){
		//make employee e1
		char e1_first_name[MAXNAME];
		char e1_last_name[MAXNAME];
		int e1_id;
		int e1_salary;
		
		bool proper_format = false;
		
		//Each do-while loop asks for imput and ensures correct format
		//Get first name
		do{
			proper_format = false;
			printf("Enter first name\n");
			scanf("%s", e1_first_name);
			
			if(strlen(e1_first_name) > 0 && strlen(e1_first_name)<65)
				proper_format = true;
			else
				printf("Improper format: Name must be less than 64 characters\n");
		}while (!proper_format);
		
		//Get last name
		do{
			proper_format = false;
			printf("Enter last name\n");
			scanf("%s", e1_last_name);
			
			if(strlen(e1_last_name) > 0 && strlen(e1_last_name)<65)
				proper_format = true;
			else
				printf("Improper format: Name must be less than 64 characters\n");
		}while (!proper_format);
		
		//Get id
		do{
			proper_format = false;
			
			printf("Enter 6 digit id number\n");
			scanf("%d", &e1_id);
			
			if(e1_id >= 100000 && e1_id <= 999999)
				proper_format = true;
			else
				printf("Improper format: ID must be 6 digits\n");
		}while (!proper_format);
		
		//Get salary
		do {
			proper_format = false;
			
			printf("Enter salary\n");
			scanf("%d", &e1_salary);
			
			if(e1_salary >= 30000 && e1_salary <= 150000)
				proper_format = true;
			else
				printf("Improper format: Salary must be between $30,000 and $150,000\n");
		}while (!proper_format);
		
		//Fill employee data
		strcpy(e1.first_name, e1_first_name);
		strcpy(e1.last_name, e1_last_name);
		e1.id = e1_id;
		e1.salary = e1_salary; 
		
		
		//Employee e1 created!
		
		//Confirm
		printf("Are you sure you want to add? (y/n) \n");
		printf("------------------------------------------------------\n");
		displayEmployeeInformation(e1);
		printf("------------------------------------------------------\n");
		scanf(" %c", &answer);
		if (answer == 'y')
			break;
	}
	
	//Add employee to Database and re-sort database 
	db[number_of_employees] = e1;
	printf("Employee Successfully Added!\n");
	quicksort(db, 0, number_of_employees);
}

/*
 * This function uses a generic quicksort algorithm to sort the database
 * @param db employee database
 * @param first, last : positions of array
 * @return void
 */

void quicksort(struct Employee db[], int first, int last){
    int pivot,j,i;
	struct Employee temp;

     if(first<last){
         pivot=first;
         i=first;
         j=last;

         while(i<j){
             while( (db[i].id) <= (db[pivot].id) && i<last)
                 i++;
             while((db[j].id) > (db[pivot].id))
                 j--;
             if(i<j){
                  temp=db[i];
                  db[i]=db[j];
                  db[j]=temp;
             }
         }
         temp=db[pivot];
         db[pivot]=db[j];
         db[j]=temp;
         quicksort(db,first,j-1);
         quicksort(db,j+1,last);
    }
}


/*
 * This function iterates through array to calculate total number of employees in database
 * @param db employee database
 * @return int i: total number of employees
 */
int getNumberOfEmployees(struct Employee db[]){
	int i = 0;
	while(db[i].id != 0) i++;
	return i;
}


/*
 * DR. SAT
 *
 * This is a function to test the readfile library on a DB input file: you will
 * not really call this in your program, but use it as an example of how to use
 * the readfile library functions.
 * @param filename the filename to open and read
 */
void readAndPrintFile(char *filename) {
	printf("filename '%s'\n", filename);

	int ret = openFile(filename);
	if (ret == -1) {
		printf("error: can't open %s\n", filename);
		exit(1);
	}

	int id, salary;
	char fname[MAXNAME], lname[MAXNAME];

	while (ret != -1) {    
		ret = readInt(&id);
		if (ret) { break; }
		ret = readString(fname);
		if (ret) { break; }
		ret = readString(lname);
		if (ret) { break; }
		ret = readInt(&salary);
		if (ret == 0) {
			printf("%d %s %s %d\n", id, fname, lname, salary);

		}
	}

	closeFile();  // when all done close the file
}

/*
 *  DR. SAT
 *  DO NOT MODIFY THIS FUNCTION. It works "as is".
 *
 *  This function gets the filename passed in as a command line option
 *  and copies it into the filename parameter. It exits with an error 
 *  message if the command line is badly formed.
 *  @param filename the string to fill with the passed filename
 *  @param argc, argv the command line parameters from main 
 *               (number and strings array)
 */
void getFilenameFromCommandLine(char filename[], int argc, char *argv[]) {

	if (argc != 2) {
		printf("Usage: %s database_file\n", argv[0]);
		// exit function: quits the program immediately...some errors are not 
		// recoverable by the program, so exiting with an error message is 
		// reasonable error handling option in this case
		exit(1);   
	}
	if (strlen(argv[1]) >= MAXFILENAME) { 
		printf("Filename, %s, is too long, cp to shorter name and try again\n",
				filename);
		exit(1);
	}
	strcpy(filename, argv[1]);
}
